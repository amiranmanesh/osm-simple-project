package amiranmanesh.osm

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import kotlinx.android.synthetic.main.activity_main.*
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.overlay.ItemizedIconOverlay
import org.osmdroid.views.overlay.ItemizedOverlayWithFocus
import org.osmdroid.views.overlay.OverlayItem
import org.osmdroid.views.overlay.compass.CompassOverlay
import org.osmdroid.views.overlay.MinimapOverlay
import org.osmdroid.views.overlay.gestures.RotationGestureOverlay





class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //init
        Configuration.getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this))
        Configuration.getInstance().userAgentValue = BuildConfig.APPLICATION_ID


        mapView.setTileSource(TileSourceFactory.DEFAULT_TILE_SOURCE)
        mapView.setMultiTouchControls(true)
        val mapController = mapView.controller
        mapController.setZoom(16)

        //to set this location in the center of view
        val startPoint = GeoPoint(36.288164, 59.616059)
        mapController.setCenter(startPoint)

        //to add Compass
        val compassOverlay = CompassOverlay(this, mapView)
        compassOverlay.enableCompass()
        mapView.overlays.add(compassOverlay)


        //to mark places with customize marker
        //your items
        val items = ArrayList<OverlayItem>()
        val temp = OverlayItem("title", "description", GeoPoint(36.288164, 59.616059))
        temp.setMarker(resources.getDrawable(R.drawable.ic_pin))
        items.add(temp)

        val mOverlay = ItemizedOverlayWithFocus(
            items,
            object : ItemizedIconOverlay.OnItemGestureListener<OverlayItem> {
                override fun onItemSingleTapUp(index: Int, item: OverlayItem): Boolean {
                    //do something
                    return true
                }

                override fun onItemLongPress(index: Int, item: OverlayItem): Boolean {
                    return false
                }
            }, this
        )
//        mOverlay.setFocusItemsOnTap(true)

        mapView.overlays.add(mOverlay)

        //to add mini map on screen
        val mMiniMapOverlay = MinimapOverlay(this, mapView.tileRequestCompleteHandler)
        mMiniMapOverlay.width = 500
        mMiniMapOverlay.height = 300
        mapView.overlays.add(mMiniMapOverlay)

        //to enable rotation
        val mRotationGestureOverlay = RotationGestureOverlay(mapView)
        mRotationGestureOverlay.isEnabled = true
        mapView.overlays.add(mRotationGestureOverlay)

    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

}
